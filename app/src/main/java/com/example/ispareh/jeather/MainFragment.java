package com.example.ispareh.jeather;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainFragment extends Fragment {

    private String TAG = this.getClass().getCanonicalName();
    private ArrayAdapter<String> adapter;
    private ListView myListView;
    public MainFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        myListView = (ListView) rootView.findViewById(R.id.listView);
        GetWeatherTask getWeatherTask = new GetWeatherTask();
        getWeatherTask.execute("Isfahan,Ir");
        return rootView;
    }

    public class GetWeatherTask extends AsyncTask<String, Void, String[]>{

        @Override
        protected void onPostExecute(String[] strings) {
            super.onPostExecute(strings);

            adapter = new ArrayAdapter<String>(getActivity(), R.layout.list_item, strings);
            myListView.setAdapter(adapter);
        }

        @Override
        protected String[] doInBackground(String... params) {
            HttpURLConnection urlConnection = null;
            BufferedReader bufferedReader = null;
            String jsonString = null;
            String[] forecastResults = null;

            try{
                URL url = new URL("http://api.openweathermap.org/data/2.5/forecast/daily?q=Isfahan,IR&mode=json&units=metric&cnt=7");
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                StringBuffer buffer = new StringBuffer();
                InputStream inputStream = urlConnection.getInputStream();

                if(inputStream == null) {
                    return null;
                }

                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String line = null;
                while (  (line = bufferedReader.readLine()) != null ) {
                    Log.v(TAG, line);
                    buffer.append(line + "\r\n");
                }

                if (buffer.length() == 0) {
                    return null;
                }

                jsonString = buffer.toString();
//                jsonString = "{\"cod\":\"200\",\"message\":0.0262," +
//                        "\"city\":{\"id\":\"418863\",\"name\":\"Esfahan\",\"coord\":{\"lon\":51.6695,\"lat\":32.6578}," +
//                        "\"country\":\"Iran\",\"population\":0},\"cnt\":7," +
//                        "\"list\":[" +
//                            "{\"dt\":1430380800,\"temp\":" +
//                                "{\"day\":24.05,\"min\":6.75,\"max\":24.23,\"night\":6.75,\"eve\":19.05,\"morn\":24.05}," +
//                                "\"pressure\":816.7,\"humidity\":19," +
//                                "\"weather\":[{\"id\":800,\"main\":\"Clear\",\"description\":\"sky is clear\",\"icon\":\"01d\"}]," +
//                                "\"speed\":2.28,\"deg\":209,\"clouds\":0},{\"dt\":1430467200," +
//                                "\"temp\":{\"day\":25.8,\"min\":7.99,\"max\":25.8,\"night\":9.27,\"eve\":20.1,\"morn\":7.99},\"pressure\":816.14,\"humidity\":20," +
//                                "\"weather\":[{\"id\":800,\"main\":\"Clear\",\"description\":\"sky is clear\",\"icon\":\"02d\"}]," +
//                                "\"speed\":3.01,\"deg\":217,\"clouds\":8}," +
//                           "{\"dt\":1430553600,\"temp\":{\"day\":24.06,\"min\":8.41,\"max\":24.06,\"night\":10.98,\"eve\":20.11,\"morn\":8.41},\"pressure\":814.78,\"humidity\":20,\"weather\":[{\"id\":804,\"main\":\"Clouds\",\"description\":\"overcast clouds\",\"icon\":\"04d\"}],\"speed\":1.67,\"deg\":235,\"clouds\":88},{\"dt\":1430640000,\"temp\":{\"day\":23.71,\"min\":14.36,\"max\":25.53,\"night\":16.33,\"eve\":25.53,\"morn\":14.36},\"pressure\":836.78,\"humidity\":0,\"weather\":[{\"id\":500,\"main\":\"Rain\",\"description\":\"light rain\",\"icon\":\"10d\"}],\"speed\":2.39,\"deg\":218,\"clouds\":0,\"rain\":0.44},{\"dt\":1430726400,\"temp\":{\"day\":22.86,\"min\":7.86,\"max\":26.19,\"night\":13.05,\"eve\":26.19,\"morn\":7.86},\"pressure\":836.96,\"humidity\":0,\"weather\":[{\"id\":800,\"main\":\"Clear\",\"description\":\"sky is clear\",\"icon\":\"01d\"}],\"speed\":1.32,\"deg\":229,\"clouds\":0},{\"dt\":1430812800,\"temp\":{\"day\":19.52,\"min\":7.12,\"max\":24.54,\"night\":16.84,\"eve\":24.54,\"morn\":7.12},\"pressure\":836.42,\"humidity\":0,\"weather\":[{\"id\":500,\"main\":\"Rain\",\"description\":\"light rain\",\"icon\":\"10d\"}],\"speed\":1.67,\"deg\":286,\"clouds\":96,\"rain\":0.71},{\"dt\":1430899200,\"temp\":{\"day\":22.63,\"min\":12.26,\"max\":25.1,\"night\":14.48,\"eve\":25.1,\"morn\":12.26}," +
//                        "\"pressure\":836.21,\"humidity\":0,\"weather\":[{\"id\":500,\"main\":\"Rain\",\"description\":\"light rain\",\"icon\":\"10d\"}],\"speed\":1.67,\"deg\":270,\"clouds\":0,\"rain\":1.38}]}";
                JSONObject jsonObject = new JSONObject(jsonString);
                JSONArray days = jsonObject.getJSONArray("list");
                forecastResults = new String[days.length()];
                for(int i = 0; i<= days.length(); i++) {
                    JSONObject jsonDay = days.getJSONObject(i);
                    String dateTime = this.getDate(jsonDay.getLong("dt") * 1000);
                    Log.e("dateTime",dateTime);
                    JSONObject tempInfo = jsonDay.getJSONObject("temp");

                    String temp = Double.toString(tempInfo.getDouble("day"));
                    String min = Double.toString(tempInfo.getDouble("min"));
                    String max = Double.toString(tempInfo.getDouble("max"));

                    String today = dateTime + "\n\nDay : "+temp + " | Min : " + min +  " | Max : "  + max;
                    Log.e("days",today);
                    forecastResults[i] = today;
                }
                inputStream.close();
//                return forecastResults;

            }
            catch (IOException e) {
                Log.e("PlaceholderFragment", "Error", e);
            }
            catch (JSONException e) {
                e.printStackTrace();
            } finally {
                if(urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (final IOException e) {
                        Log.e("PlaceholderFragment", "Error closing stream", e);
                    }
                }
            }

            return forecastResults;
        }

        private String getDate(Long timestamp) {
            try {
                DateFormat dateFormat = new SimpleDateFormat("MM/dd");
                Date newDate = new Date(timestamp);
                return dateFormat.format(newDate);
            }catch (Exception e){
                Log.e("Date error",e.toString());
                return "xxxxx";
            }
        }
    }

}